# -*- coding: utf-8 -*-
from __future__ import with_statement, print_function, absolute_import

from trello import TrelloBase
from trello.compat import force_str


class Comment(TrelloBase):
    """
    Class representing a Trello Comment.
    """

    def __init__(self, client, obj, trello_card=None):
        super(Comment, self).__init__()
        self.client = client
        self.trello_card = trello_card
        self.id = obj['id']
        self.idMemberCreator = obj['idMemberCreator']
        self.memberCreator = obj['memberCreator']['fullName']
        self.text = obj['data']['text']
        self.date = obj['date']
        self.rawData = obj
