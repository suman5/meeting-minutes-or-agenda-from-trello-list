from dateutil import parser
import pytz
import os,sys
from trello import TrelloClient
import datetime
import time
import json

config = json.load(open('config.json', 'r'))

client = TrelloClient(
    api_key=config['api_key'],
    api_secret=config['api_secret'],
    token=config['token']#,
    #token_secret='your-oauth-token-secret'
)

date_entry = input('Enter a Meeting Date in YYYY-MM-DD format: ')
year, month, day = map(int, date_entry.split('-'))
date1 = datetime.date(year, month, day)

with open('mom/mom{}.md'.format(date_entry), 'w') as f:
    f.writelines('\\newpage\n\n# Meeting Minutes (Date: {:%d/%b/%Y})'.format(date1))

    #cc_board = client.get_board('5f3d12eb912e795ed3e4c7c1')
    #weekly meeting list id
    weekly_meeting_list = client.get_list(config['list_id'])
    count = 1
    for c in weekly_meeting_list.list_cards():    
        f.writelines('\n\n## Agenda: {}\n\n'.format(c.name))

        if(len(c.labels) > 0):
            for l in c.labels:
                f.writelines('''\\ttag{{{0}}}{{{1}}} '''.format(l.color, l.name))
            f.writelines('\n\n')

        f.writelines('**Description:** {}\n\n'.format(c.description))
        
        if(len(c.checklists)>0):
            f.writelines('### Action Items:\n\n')
            cnt = 0
            for cl in c.checklists:
                cnt += 1
                f.writelines('\n\n{}. {}\n\n'.format(cnt, cl.name))
                for i in cl.items:
                    f.writelines('- [{}] {}\n'.format('X' if i['checked'] else ' ', i['name']))
            f.writelines('\n')
        
        if(len(c.comments)>0):
            f.writelines('### Comments/Updates:\n\n')
            for com in c.comments:
                f.writelines('***{}*** ({} IST): {}\n\n'.format(com.memberCreator, parser.isoparse(com.date)
                                                                .astimezone(pytz.timezone('Asia/Kolkata'))
                                                                .strftime('%d/%b/%Y %H:%M'), 
                                                                com.text))
            
        mname = []
        for mid in c.member_ids:
            m = client.get_member(mid)
            mname.append(m.full_name)

        if(len(mname)>0):
            f.writelines('**Assigned to:** ')
            f.writelines('*{}* '.format(', '.join(mname)))

        count += 1
