from dateutil import parser
import pytz
import os,sys
from trello import TrelloClient
import datetime
import time
import json

config = json.load(open('config.json', 'r'))

client = TrelloClient(
    api_key=config['api_key'],
    api_secret=config['api_secret'],
    token=config['token']#,
    #token_secret='your-oauth-token-secret'
)

for b in client.list_boards():
    print("Board: {}, ID: {}:".format(b.name, b.id))
    for l in b.list_lists():
        print("\tList: {}, ID: {}".format(l.name, l.id))