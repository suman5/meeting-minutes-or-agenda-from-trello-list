## MOM Generation from Trello List

This is a small tool which will create a journal file out of a trello list. Each time you run the code, it will ask for a date. This date is the meeting date but do not have any filtering associated with it. The date will be printed in the pdf for distingusiong it from the other minutes.

Once entered, the code will fetch all the cards in the list and each card is considerd to be a Agenda Item. It then creates a markdown file in mom directory. It uses py-trello (modified, included in the folder) and https://github.com/TheFern2/markdown-book for creation of the pdf file. You need pandoc and xelatex installed in the system to generate the pdf. Markdown file can be generated without it.

### Configuration

A configuration template is provided in the root directory. The API Key and other authentication data can be obtained from https://trello.com/app-key. Rename the template file to config.json after filling the data.

For getting the list id, run `python get_list_ids.py` from the command prompt. All the lists of all the board of the user will be listed. Please note that the authentication details of the config must be filled before running this command.

Title/Authors and color of the links and section heading can be modified in the title.txt file. You may need to have knowledge of LaTeX to interpet a few details there.

### Running the code

To generate md file from trello run `python md_from_trello.py`. To generate pdf from the md file of mom folder run `python export_book.py -p mom`.

Single command to run the both: `sh ./create_pdf.sh`.